# Install Eclipse Neon 2
Eclipse for Java Developers

# install FRC Suite
Create an account ni.com and
Download the suite: http://www.ni.com/download/first-robotics-software-2017/7183/en/
Unzip it using the password unveiled during the FRC kick off
Use the serial number located on the envelope from the Kit of Parts


# Create a new project in GitLab
go to gitlab.com
sign in
create a new project: i.e. Robot2019

# Install Git on windows
Download Git an install it: https://git-scm.com/downloads
Go to your user folder and create a 'git' folder (it will contains all the git projects)
cd git
git clone https://gitlab.com/<USERNAME>/Robot2019

# Install eGit in Eclipse

# Add Git repositories in Eclipse
From Git Repository view
* Pick the icon "Clone a Git Repository and  add a clone to this view"
* URI: Paste the URL from Gitlab: https://gitlab.com/xxxx/xxxx.git
* Next
* Tick "master"
* Local Destination: C:\Users\xxxx\git\2018-xxxx
* Finish



# Copy project files from last year
duplicate the source code from last year into the new git project
you might need to edit the project name: .project file
'''
<name>2018-CompetitionRobot</name>
'''


# Import Git Project in Eclipse
Go to File/Import, 
General/Existing Project into Workspace, 
Select root directory: C:\Users\DMRobotics\git\2018-PowerUp
The Eclipse project "2018-CompetitionRobot" should show-up
Clikc it and import

# Compile
In the Java perspective,
Open folders to reach Robot.java
Right-click on Robot.java 
Run As WPILib Java Deploy

